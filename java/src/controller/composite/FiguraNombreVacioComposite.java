package objetosEnClase.controller.composite;

public class FiguraNombreVacioComposite extends ValidatorComposite {
    @Override
    public boolean validar() {
        return figura.getNombre().isEmpty();
    }
    public FiguraNombreVacioComposite() {
    }
    @Override
    public String getError() {
        return "El nombre no puede estar vacio";
    }
    @Override
    public boolean isMe() {
        return true;
    }

}
