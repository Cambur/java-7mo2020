package objetosEnClase.controller.composite;

import objetosEnClase.modelo.Cuadrado;

public class CuadradoLadoNegativoCeroComposite extends ValidatorComposite {
    @Override
    public boolean isMe() {
        return figura instanceof Cuadrado;
    }    @Override
    public String getError() {
        return "El lado debe ser mayor que cero";
    }
    public CuadradoLadoNegativoCeroComposite() {
    }
    @Override
    public boolean validar() {
        Cuadrado cua = (Cuadrado) figura;
        return cua.getLado() <= 0;
    }

}
