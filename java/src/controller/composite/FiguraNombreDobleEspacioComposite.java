package objetosEnClase.controller.composite;

public class FiguraNombreDobleEspacioComposite extends ValidatorComposite {
    @Override
    public String getError() {
        return "El nombre no puede tener dos espacios";
    }
    public FiguraNombreDobleEspacioComposite() {
    }
    @Override
    public boolean validar() {
        return figura.getNombre().contains("  ");
    }
    @Override
    public boolean isMe() {
        return true;
    }
}
