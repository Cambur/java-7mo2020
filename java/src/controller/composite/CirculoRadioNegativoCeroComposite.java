package objetosEnClase.controller.composite;

import objetosEnClase.modelo.Circulo;

public class CirculoRadioNegativoCeroComposite extends ValidatorComposite {
    @Override
    public boolean validar() {
        Circulo cir = (Circulo) figura;
        return cir.getRadio() <= 0;
    }
    public CirculoRadioNegativoCeroComposite() {
    }
    @Override
    public String getError() {
        return "El radio debe ser mayor que cero";
    }
    @Override
    public boolean isMe() {
        return figura instanceof Circulo;
    }
}
